# YOLOv5 tuned
This repository is the Yolov5 model, that has been modified in order to
train with RGBD images from a wine yard dataset.

To train, create dataset and test the code, the original instructions has to
be followed: [Link](https://github.com/ultralytics/yolov5)

## Additional options:
During testing, we have to call the detect algorithm like this:
```
python3 detect.py --source IMAGE_TO_TEST --depth_imgs DEPTH_IMGS_DIR --weights TRAINED_PT_FILE
```

The best training configuration I found is:
```
python3 train_yolo.py --img 640 --batch 4 --epochs 100 --data custom_dataset.yaml --weights yolov5m.pt --adam
```